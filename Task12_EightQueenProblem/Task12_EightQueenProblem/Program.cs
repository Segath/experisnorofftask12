﻿using System;

namespace Task12_EightQueenProblem
{
    class Program
    {
        static void Main(string[] args)
        {
            char[,] chessBoard = new char[8, 8];
            Console.Write("Type in column position (a-h) ");
            char col = char.Parse(Console.ReadLine());

            Console.Write("Type in row position (1-8) ");
            int row = int.Parse(Console.ReadLine()) - 1;



            EightQueenAlgorithm(chessBoard, row, CharToIntPosition(col));
        }

        // Converts the a-h positioning of chessboards to locations in an array
        public static int CharToIntPosition(char c)
        {
            switch (c)
            {
                case 'a':
                    return 0;
                case 'b':
                    return 1;
                case 'c':
                    return 2;
                case 'd':
                    return 3;
                case 'e':
                    return 4;
                case 'f':
                    return 5;
                case 'g':
                    return 6;
                case 'h':
                    return 7;
            }
            return -1;
        }

        public static char IntToCharPosition(int i)
        {
            switch (i)
            {
                case 0:
                    return 'a';
                case 1:
                    return 'b';
                case 2:
                    return 'c';
                case 3:
                    return 'd';
                case 4:
                    return 'e';
                case 5:
                    return 'f';
                case 6:
                    return 'g';
                case 7:
                    return 'h';
            }
            return '\0';
        }


        // Checks if there is a queen on the same row, column or diagonal in the specified point
        public static bool IsThereAQueen(char[,] chessBoard, int row, int col)
        {
            // Check the row and column
            for (int i = 0; i < 8; i++)
            {
                if(chessBoard[row, i] == 'Q')
                {
                    return true;
                }
                else if(chessBoard[i, col] == 'Q')
                {
                    return true;
                }
            }

            // Check diagonal
            // Top left -> bottom right
            for (int i = 1; i < 8; i++)
            {
                // Try to block towards top left
                if (col - i >= 0 && row - i >= 0)
                {
                    if(chessBoard[row - i, col - i] == 'Q')
                    {
                        return true;
                    }
                }
                // Try to block towards bottom right
                if (col + i < 8 && row + i < 8)
                {
                    if (chessBoard[row + i, col + i] == 'Q')
                    {
                        return true;
                    }
                }

                // Try to block towards bottom left
                if (col - i >= 0 && row + i < 8)
                {
                    if (chessBoard[row + i, col - i] == 'Q')
                    {
                        return true;
                    }
                }
                // Try to block towards top right
                if (col + i < 8 && row - i >= 0)
                {
                    if (chessBoard[row - i, col + i] == 'Q')
                    {
                        return true;
                    }
                }
            }
            return false;

        }


        // Find valid positions on the chessboard to fulfill the 8 queens problem
        public static void EightQueenAlgorithm(char[,] chessBoard, int initialRow, int initialColumn)
        {
            // Place the first queen at the designated position
            chessBoard[initialRow, initialColumn] = 'Q';

            // Checks if a queen has been placed
            bool placedQueen;


            // Col
            for (int i = 0; i < 8; i++)
            {
                placedQueen = false;
                
                // Row
                for (int j = 0; j < 8; j++)
                {
                    // Skip the column containing the initial queen
                    if (i == initialColumn)
                    {
                        placedQueen = true;
                        break;
                    }

                    // Checks if there is a valid placement, and there is not an X in the way
                    // The X is a placeholder to signify that there has been a queen there before.
                    if(!IsThereAQueen(chessBoard, j, i) && chessBoard[j, i] != 'X')
                    {
                        chessBoard[j, i] = 'Q';
                        placedQueen = true;
                        break;
                    }
                }

                // If a queen has not been placed on a column, backtrack and try again
                if (!placedQueen)
                {
                    // Remove X from current column in order to test new positions again.
                    for (int j = 0; j < 8; j++)
                    {
                        if (chessBoard[j, i] == 'X')
                        {
                            chessBoard[j, i] = '\0';
                        }
                    }

                    // Go one step back
                    if (i - 1 >= 0)
                    {
                        i--;

                        // If we land on the initial queens column, then go back one more step
                        if(i == initialColumn && initialColumn != 0)
                        {
                            i--;
                        }
                    }


                    // Replace Queen with X in order to signify that a queen have been tested on that position
                    for (int j = 0; j < 8; j++)
                    {
                        if (chessBoard[j, i] == 'Q')
                        {
                            chessBoard[j, i] = 'X';
                        }
                    }


                    // Go one step back at the end, because of the ++ in the for loop
                    // -1 in the check because the for loop will make it a 0.
                    if(i - 1 >= -1)
                    {
                        i -= 1;
                    }
                }
            }

            Console.WriteLine($"Found a solution at: {IntToCharPosition(initialColumn)}{initialRow+1}");
            Console.WriteLine();
            PrintBoard(chessBoard);
        }

        public static void PrintBoard(char[,] chessBoard)
        {
            char piece;

            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if(chessBoard[i,j] == 'Q')
                    {
                        piece = 'Q';
                    }
                    else
                    {
                        piece = '\0';
                    }
                    Console.Write("|" + piece);
                    Console.Write(" ");
                    if(j == 7)
                    {
                        Console.Write("| " + (i + 1));
                    }
                }
                Console.WriteLine();
            }

            Console.WriteLine();
            for (int i = 0; i < 8; i++)
            {
                Console.Write(" " + IntToCharPosition(i) + " ");
            }
            Console.WriteLine();
        }
    }
}
